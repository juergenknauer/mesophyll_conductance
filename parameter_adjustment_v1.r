#############################
#### Parameter adjustment ###-------------------------------------------------------------------------------
#############################

#' Parameter adjustment of photosynthetic capacity for C3 vegetation.
#' 
#' @description Adjusts C3 photosynthetic parameters from their apparent (Ci-based) to true (Cc-based)
#'              values. 
#'                            
#' @param Ci            Leaf intercellular CO2 concentration (umol mol-1)                            
#' @param Vcmax25_Ci    Ci-based (apparent) maximum carboxylation rate at 25 degC (umol m-2 s-1)         
#' @param Jmax25_Ci     Ci-based (apparent) maximum electron transport rate at 25 degC (umol m-2 s-1)
#' @param gmmax25       Unstressed mesophyll conductance to CO2 transfer at 25 degC (mol m-2 s-1)
#' @param Rd            Day respiration (umol m-2 s-1)
#' @param Kc_ci         Ci-based Michaelis-Menten constant for CO2 at 25 degC (umol mol-1)
#' @param Ko_ci         Ci-based Michaelis-Menten constant for O2 at 25 degC (mmol mol-1)
#' @param Gam_ci        Ci-based photorespiratory CO2 compensation point ('Gamma star') 
#'                      at 25 degC (umol mol-1)
#' @param Kc_cc         Cc-based Michaelis-Menten constant for CO2 at 25 degC (umol mol-1)
#' @param Ko_cc         Cc-based Michaelis-Menten constant for O2 at 25 degC (mmol mol-1)
#' @param Gam_cc        Cc-based photorespiratory CO2 compensation point ('Gamma star') 
#'                      at 25 degC (umol mol-1)
#' @param Oi            Intercellular O2 concentration (mol mol-1)
#' @param Ci_response   Does gm respond to intercellular CO2 concentration? Defaults to \code{FALSE}. 
#' @param all_Ci_based  Take ci-based Rubisco kinetic constants also for the Cc-based case? Defaults to \code{FALSE}
#' @param eval_ci_range A vector of length 2, indicating the minimum and maximum value of the Ci range for which the
#'                      agreement between the gm-implicit and gm-explicit An-Ci curves is evaluated. 
#'                      Defaults to \code{c(0,1500)}.
#' @param model         Photosynthesis model. At the moment only \code{"Farquhar_1980_ML"}, the mono-limiting 
#'                      biochemical photosynthesis model by Farquhar et al. 1980 (without TPU limitation) is implemented. 
#' 
#' @details 
#' 
#' 
#' 
#' @return a list with the following elements:
#'         \item{Vcmax25_Cc}{Cc-based (apparent) maximum carboxylation rate (Vcmax) at 25 degC (umol m-2 s-1)}
#'         \item{Jmax25_Cc}{Cc-based (apparent) maximum electron transport rate (Jmax) at 25 degC (umol m-2 s-1)}
#'         \item{Jmax_Vcmax_ratio}{Ratio of Cc-based Jmax to Cc-based Vcmax at 25 degC}
#'         \item{eval_metrics}{Metrics evaluationg the difference between the implicit and explicit An-Ci curves.
#'                             At the moment, RMSE (root mean square error) and MAE (mean absolute error) are implemented}
#' 
#' @references Knauer et al. 2019: Effects of mesophyll conductance on vegetation responses to elevated
#'             CO2 concentrations in a land surface model. Global Change Biology.
#' 
#'             Farquhar, G. von Caemmerer, S., Berry, J.A. 1980:
#'             A biochemical model of photosynthetic CO2 assimilation in leaves of C3 species.
#'             Planta, Springer, 1980, 149, 78-90.
#' 
#'             Bernacchi, C.J., Singsaas E.L., Pimentel C., Portis JR A.R., Long S.P., 2001:
#'             Improved temperature response functions for models of Rubisco-limited
#'             photosynthesis. Plant, Cell and Environment 24, 253-259. 
#'             
#'             Bernacchi, C.J., Portis, A.R., Nakano, H., von Caemmerer, S.,  Long, S.P. 2002:
#'             Temperature response of mesophyll conductance. Implications for the determination of
#'             Rubisco enzyme kinetics and for limitations to photosynthesis in vivo Plant Physiology, 
#'             Am Soc Plant Biol, 130, 1992-1998.
#'             
#' @examples 
#' 
#' Ci <- seq(0,1500,1)   # range of Ci used for fitting
#' 
#' Vcmax25_Ci <- 45
#' Jmax25_Ci  <- 1.9 * Vcmax25_Ci  
#' 
#' Rd        <- 0.011 * Vcmax25_Ci
#' gm_max25  <- 0.1  # mol m-2 s-1
#' 
#' 
#' adjust.param.C3(Ci,Vcmax25_Ci,Jmax25_Ci,gmmax25=gm_max25,Rd=Rd,
#'                 Kc_ci=404.9,Ko_ci=278.4,Gam_ci=42.75,
#'                 Kc_cc=272.38,Ko_cc=165.82,Gam_cc=37.43,
#'                 Oi=0.21,Ci_response=FALSE,all_Ci_based=FALSE,
#'                 eval_ci_range=c(0,1500),model="Farquhar_1980_ML")
#' 
#' 
#' @export
adjust.param.C3 <- function(Ci,Vcmax25_Ci,Jmax25_Ci,gmmax25,Rd,
                            Kc_ci=404.9,Ko_ci=278.4,Gam_ci=42.75,
                            Kc_cc=272.38,Ko_cc=165.82,Gam_cc=37.43,
                            Oi=0.21,Ci_response=FALSE,all_Ci_based=FALSE,
                            eval_ci_range=c(0,1500),model="Farquhar_1980_ML"){ 
  
  model <- match.arg(model)
  
  Ko_ci <- Ko_ci * 1000
  Ko_cc <- Ko_cc * 1000
  Oi    <- Oi * 1e06
  
  ### 1) calculate unstressed An-Ci response curve at 25 degC and saturated light
  J1 <- Jmax25_Ci  # J1 is assumed to equal Jmax25!
  Je <- J1 * (Ci - Gam_ci)/4/(Ci + 2*Gam_ci)
  Jc <- Vcmax25_Ci * (Ci - Gam_ci)/(Ci + Kc_ci * (1 + Oi/Ko_ci))
  
  Ag <- pmin(Je,Jc)
  An <- Ag - Rd
  
  Rubisco_lim <- which(Jc <= Je)
  RuBP_lim    <- which(Je < Jc)
  
  An[An < 0] <- NA
  
  
  
  ### 2) calculate Cc based on gm
  gm <- gmmax25
  
  if (Ci_response){
    Cc <- Ci - An / (gm * (0.15 + 1.5* (1-exp(-Ci/38)) * exp(-Ci/460)))
  } else { # default case: gm is constant with ci
    Cc <- Ci - An / gm
  }
  
  # constrain Cc > 0
  if (any(Cc < 0,na.rm=T)){
    warning("Cc falls below 0!!",immediate.=TRUE)
    Cc <- pmax(0,Cc)
  }
  
  
  
  ### 3) refit Vcmax and Jmax
  if (all_Ci_based){  # take Ci-based Rubisco kinetic constants also for the Cc-based case
    Gam_cc <- Gam_ci
    Kc_cc  <- Kc_ci
    Ko_cc  <- Ko_ci
  }
  

  mod <- nls(An ~ c(pmin(J1 * (Cc - Gam_cc)/4/(Cc + 2*Gam_cc),Vcmax * (Cc - Gam_cc)/(Cc + Kc_cc * (1 + Oi/Ko_cc))) - Rd),
             start=c(Vcmax=Vcmax25_Ci,J1=J1),algorithm="port")
    

  Vcmax25_Cc <- summary(mod)$coef[1,1]
  Jmax25_Cc  <- summary(mod)$coef[2,1]
  Ratio      <- Jmax25_Cc/Vcmax25_Cc   
  

  
  
  ### 4) recalculate the An-Ci curve and evaluate the fit 
  Je2 <- Jmax25_Cc * (Cc - Gam_cc)/4/(Cc + 2*Gam_cc)
  Jc2 <- Vcmax25_Cc * (Cc - Gam_cc)/(Cc + Kc_cc * (1 + Oi/Ko_cc))
  
  Ag2 <- pmin(Je2,Jc2)
  An2 <- Ag2 - Rd
  
  
  
  Ci_eval <- Ci >= eval_ci_range[1] & Ci <= eval_ci_range[2]
  
  # root mean square error
  RMSE <- sqrt(mean((An2[Ci_eval] - An[Ci_eval])^2,na.rm=TRUE))
  
  # mean absolute error
  MAE  <- mean(abs(An2[Ci_eval] - An[Ci_eval]),na.rm=TRUE)
  
  eval_metrics        <- list(RMSE,MAE)
  names(eval_metrics) <- c("RMSE","MAE")
  
  return(list("Vcmax25_Cc"=Vcmax25_Cc,"Jmax25_Cc"=Jmax25_Cc,"Jmax_Vcmax_ratio"=Ratio,
              "eval_metrics"=eval_metrics))
  
}







#' Parameter adjustment of photosynthetic capacity for C4 vegetation.
#' 
#' @description Adjusts C4 photosynthetic parameters from their apparent (Ci-based) to true (Cc-based)
#'              values. 
#'                            
#' @param Ci            Leaf intercellular CO2 concentration (umol mol-1)                            
#' @param Vcmax25_Ci    Ci-based (apparent) maximum carboxylation rate at 25 degC (umol m-2 s-1)         
#' @param Jmax25_Ci     Ci-based (apparent) maximum electron transport rate at 25 degC (umol m-2 s-1)
#' @param Vpmax25_Ci    Ci-based (apparent) maximum PEP carboxylation rate (umol m-2 s-1)
#' @param gmmax25       Unstressed mesophyll conductance to CO2 transfer at 25 degC (mol m-2 s-1)
#' @param Rd            Day respiration (umol m-2 s-1)
#' @param k_ci          Ci-based (apparent) initial slope of the CO2 response curve (umol m-2 s-1). Only used if \code{model = "Collatz_1992"}.
#' @param Vpr           PEP regeneration rate (umol m-2 s-1). Defaults to 80 umol m-2 s-1. 
#'                      Only used if \code{model = "vonCaemmerer_1999"} 
#' @param gbs           Bundle-sheath conductance to CO2 (mol m-2 s-1). Only used if \code{model = "vonCaemmerer_1999"}
#' @param x             Partitioning factor of electron transport rate. Only used if \code{model = "vonCaemmerer_1999"}
#' @param Kp_ci         Ci-based Michaelis-Menten constant of PEP carboxylase for CO2 (umol mol-1).
#'                      Only used if \code{model = "vonCaemmerer_1999"}
#' @param Kp_cc         Cc-based Michaelis-Menten constant of PEP carboxylase for CO2 (umol mol-1).
#'                      Only used if \code{model = "vonCaemmerer_1999"}
#' @param Ci_response   Does gm respond to intercellular CO2 concentration? Defaults to \code{FALSE}. 
#' @param all_Ci_based  Take ci-based Rubisco kinetic constants also for the Cc-based case? Defaults to \code{FALSE}
#' @param eval_ci_range A vector of length 2, indicating the minimum and maximum value of the Ci range for which the
#'                      agreement between the gm-implicit and gm-explicit An-Ci curves is evaluated.
#' @param model         C4 Photosynthesis model. At the moment, either \code{"vonCaemmerer_1999"} or 
#'                      \code{"Collatz_1992"}. Note that parameter requirements depend on the model (see above).
#' 
#' @details 
#'          
#' @note      
#' Ci denotes Cm (CO2 concentration in the mesophyll cells) in C4 vegetation.
#' 
#' @return a list with the following elements:
#'         \item{Vpmax_Cc}{Cc-based maximum PEP carboxylation rate (umol m-2 s-1). Only if \code{model = "vonCaemmerer_1999"}.}
#'         \item{k_Cc}{Cc-based initial slope of the CO2 response curve (umol m-2 s-1). Only if \code{model = "Collatz_1992"}.}
#'         \item{eval_metrics}{Metrics evaluationg the difference between the implicit and explicit An-Ci curves.
#'                             At the moment, RMSE (root mean square error) and MAE (mean absolute error) are implemented}
#' 
#' @references Knauer et al. 2019: Effects of mesophyll conductance on vegetation responses to elevated
#'             CO2 concentrations in a land surface model. Global Change Biology.
#' 
#'             von Caemmerer, S., Furbank, R.T. 1999: Modeling C4 photosynthesis.
#'             In: Sage, R.F., Monson, R.K. (Eds.): C4 Plant Biology. Academic Press,
#'             Toronto, ON, Canada, 173-211.
#'             
#'             Collatz, G.J., Ribas-Carbo, M., Berry, J.: Coupled photosynthesis-stomatal conductance
#'             model for leaves of C4 plants. Functional Plant Biology, CSIRO, 1992, 19, 519-538.
#' 
#'             
#' @examples 
#' Ci <- seq(0,1500,1)   # range of Ci used for fitting
#' 
#' adjust.param.C4(Ci,Vcmax25_Ci=30,Jmax25_Ci=200,Vpmax25_Ci=60,gmmax25=1,Rd=0.3,
#'                 k_ci=0.3,Vpr=40,gbs=0.003,x=0.4,Kp_ci=80,Kp_cc=80,
#'                 Ci_response=F,eval_ci_range=c(0,1500),
#'                 model="Collatz_1992")
#' 
#' 
#' @export
adjust.param.C4 <- function(Ci,Vcmax25_Ci,Jmax25_Ci,Vpmax25_Ci,gmmax25,Rd,k_ci,
                            Vpr=80,gbs=0.003,x=0.4,Kp_ci=80,Kp_cc=80,
                            Ci_response=F,eval_ci_range,
                            model=c("vonCaemmerer_1999","Collatz_1992")){
  
  model <- match.arg(model)

  ## 1) calculate photosynthesis (An-Ci response), Ci = Cm in this case
  J1 <- Jmax25_Ci  # J1 is assumed to equal Jmax25!
  if (model == "vonCaemmerer_1999"){
    
    Vp <- pmin((Ci * Vpmax25_Ci) / (Ci + Kp_ci), Vpr)
    Ac <- pmin(Vp + gbs*Ci - 0.5*Rd,Vcmax25_Ci-Rd) 
    Aj <- pmin(((x*J1) / 2 - 0.5*Rd + gbs*Ci),(((1-x)*J1)/3) - Rd)
    
    An <- pmin(Ac,Aj)
    
  } else if (model == "Collatz_1992"){  
    
    Ac <- rep(Vcmax25_Ci,length(Ci))  - Rd
    Aj <- (0.05 * (4.6*rep(1500,length(Ci))/2.3))  - Rd # this value is much higher than the one in JSBACH; the 1500 represents APAR
    Ae <- k_ci * Ci - Rd
    
    An <- pmin(Ac,Aj,Ae)
  }
  
  Ac_lim <- which(Ac <= Aj)
  Aj_lim <- which(Aj < Ac)
  
  An[An < 0] <- NA
  
  
  
  ## 2) calculate Cc based on gm
  gm <- gmmax25
  
  if (Ci_response){
    Cc <- Ci - An / (gm * (0.15 + 1.5* (1-exp(-Ci/38)) * exp(-Ci/460)))
  } else { # default case: constant gm with ci
    Cc <- Ci - An / gm
  }
  
  ### constrain Cc > 0
  if (any(Cc < 0,na.rm=T)){
    warning("Cc falls below 0!!",immediate.=TRUE)
    Cc <- pmax(0,Cc)
  }
  
  
  
  ## 3) refit Vcmax and Jmax
  if (model == "vonCaemmerer_1999"){
      
    mod <- nls(An ~ c(pmin((pmin((Cc * Vpmax) / (Cc + Kp_cc), Vpr) + gbs*Cc - 0.5*Rd),Vcmax25_Ci - Rd)),
               start=c(Vpmax=Vpmax25_Ci),algorithm="port")   # Vcmax25_Ci would change marginally, is not fitted

    Vpmax25_Cc <- summary(mod)$coef[1,1]

    
  } else if (model == "Collatz_1992"){
    
    mod <- nls(An ~ c(pmin(k * Cc - Rd, Vcmax25_Ci - Rd)),
               start=c(k=1),algorithm="port")
    
    k_cc <- summary(mod)$coef[1,1]
    
  }
  
  

  ## 4) recalculate An using the refitted Vpmax and Vcmax
  if (model == "vonCaemmerer_1999"){
    
    Vp2 <- pmin((Cc * Vpmax25_Cc) / (Cc + Kp_cc), Vpr)
    Ac2 <- pmin(Vp2 + gbs * Cc - 0.5*Rd,Vcmax25_Ci - Rd) 
    Aj2 <- pmin(((x*J1) / 2 - 0.5*Rd + gbs * Cc),(((1 - x) * J1)/3) - Rd)   # should not have an effect
    
    An2 <- pmin(Ac2,Aj2)
    
  } else if (model == "Collatz_1992"){
    
    Ac2 <- rep(Vcmax25_Ci,length(Cc)) - Rd
    Aj2 <- (0.05 * (4.6*rep(1500,length(Cc))/2.3)) - Rd # the 1500 represents APAR
    Ae2 <- k_cc * Cc - Rd
    
    An2 <- pmin(Ac2,Aj2,Ae2)
  
  }
  
  
  Ci_eval <- Ci >= eval_ci_range[1] & Ci <= eval_ci_range[2]
  
  # root mean square error
  RMSE <- sqrt(mean((An2[Ci_eval] - An[Ci_eval])^2,na.rm=TRUE))
  
  # mean absolute error
  MAE  <- mean(abs(An2[Ci_eval] - An[Ci_eval]),na.rm=TRUE)
  
  eval_metrics        <- list(RMSE,MAE)
  names(eval_metrics) <- c("RMSE","MAE")
  
  if (model == "vonCaemmerer_1999"){
    
    return(list("Vpmax_Cc"=Vpmax25_Cc,"eval_metrics"=eval_metrics))
    
  } else if (model == "Collatz_1992"){
    
    return(list("k_cc"=k_cc,"eval_metrics"=eval_metrics))
    
  }
  
}



