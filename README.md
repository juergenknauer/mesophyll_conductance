This repository contains R code for the parameter adjustment functions described in
Knauer et al. (2019): Effects of mesophyll conductance on vegetation responses to elevated CO2 concentrations in a land surface model. 
Global Change Biology. https://doi.org/10.1111/gcb.14604 (Section 2.5) for C3 and C4 vegetation. 


The following files can be found under "Source":

**parameter_adjustment_v1.r** (March 2019):
This version is used in the publication cited above (Knauer et al. 2019, Global Change Biology).

**parameter_adjustment_v2.r** (July 2019):
This is an updated algorithm that includes an iterative procedure in the parameter adjustment, which was inadvertently missed in the first version.
This version gives marginally different results compared to the first version in most cases, but is more correct because it ensures consistency between An and Cc.
**This version should be used in future applications.**
