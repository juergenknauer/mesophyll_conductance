#############################
#### Parameter adjustment ###-------------------------------------------------------------------------------
#############################

#' Parameter adjustment of photosynthetic capacity for C3 vegetation.
#' 
#' @description Adjusts C3 photosynthetic parameters from their apparent (Ci-based) to true (Cc-based)
#'              values. 
#'                            
#' @param Ci            Leaf intercellular CO2 concentration (umol mol-1)                            
#' @param Vcmax25_Ci    Ci-based (apparent) maximum carboxylation rate at 25 degC (umol m-2 s-1)         
#' @param Jmax25_Ci     Ci-based (apparent) maximum electron transport rate at 25 degC (umol m-2 s-1)
#' @param gmmax25       Unstressed mesophyll conductance to CO2 transfer at 25 degC (mol m-2 s-1)
#' @param Rd            Day respiration (umol m-2 s-1)
#' @param Kc_Ci         Ci-based Michaelis-Menten constant for CO2 at 25 degC (umol mol-1)
#' @param Ko_Ci         Ci-based Michaelis-Menten constant for O2 at 25 degC (mmol mol-1)
#' @param Gam_Ci        Ci-based photorespiratory CO2 compensation point ('Gamma star') 
#'                      at 25 degC (umol mol-1)
#' @param Kc_Cc         Cc-based Michaelis-Menten constant for CO2 at 25 degC (umol mol-1)
#' @param Ko_Cc         Cc-based Michaelis-Menten constant for O2 at 25 degC (mmol mol-1)
#' @param Gam_Cc        Cc-based photorespiratory CO2 compensation point ('Gamma star') 
#'                      at 25 degC (umol mol-1)
#' @param Oi            Intercellular O2 concentration (mol mol-1)
#' @param Ci_response   Does gm respond to intercellular CO2 concentration? Defaults to \code{FALSE}. 
#' @param all_Ci_based  Take Ci-based Rubisco kinetic constants also for the Cc-based case? Defaults to \code{FALSE}
#' @param eval_Ci_range A vector of length 2, indicating the minimum and maximum value of the Ci range for which the
#'                      agreement between the gm-implicit and gm-explicit An-Ci curves is evaluated. 
#'                      Defaults to \code{c(0,1500)}.
#' @param maxdiff       maximum difference between Vmcax_Cc estimates. Defaults to 0.002 umol m-2 s-1.
#' @param model         Photosynthesis model. At the moment only \code{"Farquhar_1980_ML"}, the mono-limiting 
#'                      biochemical photosynthesis model by Farquhar et al. 1980 (without TPU limitation) is implemented. 
#' 
#' @details 
#' 
#' 
#' 
#' @return a list with the following elements:
#'         \item{Vcmax25_Cc}{Cc-based (apparent) maximum carboxylation rate (Vcmax) at 25 degC (umol m-2 s-1)}
#'         \item{Jmax25_Cc}{Cc-based (apparent) maximum electron transport rate (Jmax) at 25 degC (umol m-2 s-1)}
#'         \item{Jmax_Vcmax_ratio}{Ratio of Cc-based Jmax to Cc-based Vcmax at 25 degC}
#'         \item{eval_metrics}{Metrics evaluationg the difference between the implicit and explicit An-Ci curves.
#'                             At the moment, RMSE (root mean square error) and MAE (mean absolute error) are implemented}
#'                             
#' @details    Note that a combination of low gm and high Vcmax may lead to unreliable parameter values!                             
#' 
#' @references Knauer et al. 2019: Effects of mesophyll conductance on vegetation responses to elevated
#'             CO2 concentrations in a land surface model. Global Change Biology.
#' 
#'             Farquhar, G. von Caemmerer, S., Berry, J.A. 1980:
#'             A biochemical model of photosynthetic CO2 assimilation in leaves of C3 species.
#'             Planta, Springer, 1980, 149, 78-90.
#' 
#'             Bernacchi, C.J., Singsaas E.L., Pimentel C., Portis JR A.R., Long S.P., 2001:
#'             Improved temperature response functions for models of Rubisco-limited
#'             photosynthesis. Plant, Cell and Environment 24, 253-259. 
#'             
#'             Bernacchi, C.J., Portis, A.R., Nakano, H., von Caemmerer, S.,  Long, S.P. 2002:
#'             Temperature response of mesophyll conductance. Implications for the determination of
#'             Rubisco enzyme kinetics and for limitations to photosynthesis in vivo Plant Physiology, 
#'             Am Soc Plant Biol, 130, 1992-1998.
#'             
#' @examples 
#' 
#' Ci <- seq(0,1500,1)   # range of Ci used for fitting
#' 
#' Vcmax25_Ci <- 45
#' Jmax25_Ci  <- 1.9 * Vcmax25_Ci  
#' 
#' Rd        <- 0.011 * Vcmax25_Ci
#' gm_max25  <- 0.1  # mol m-2 s-1
#' 
#' 
#' adjust.param.C3(Ci,Vcmax25_Ci,Jmax25_Ci,gmmax25=gm_max25,Rd=Rd,
#'                 Kc_Ci=404.9,Ko_Ci=278.4,Gam_Ci=42.75,
#'                 Kc_Cc=272.38,Ko_Cc=165.82,Gam_Cc=37.43,
#'                 Oi=0.21,Ci_response=FALSE,all_Ci_based=FALSE,
#'                 eval_Ci_range=c(0,1500),maxdiff=0.002,
#'                 model="Farquhar_1980_ML")
#' 
#' 
#' @export
adjust.param.C3 <- function(Ci,Vcmax25_Ci,Jmax25_Ci,gmmax25,Rd,
                            Kc_Ci=404.9,Ko_Ci=278.4,Gam_Ci=42.75,
                            Kc_Cc=272.38,Ko_Cc=165.82,Gam_Cc=37.43,
                            Oi=0.21,Ci_response=FALSE,all_Ci_based=FALSE,
                            eval_Ci_range=c(0,1500),maxdiff=0.002,
                            model="Farquhar_1980_ML"){ 
  
  model <- match.arg(model)
  
  Ko_Ci <- Ko_Ci * 1000
  Ko_Cc <- Ko_Cc * 1000
  Oi    <- Oi * 1e06
  
  ### 1) calculate unstressed An-Ci response curve at 25 degC and saturated light
  J1    <- Jmax25_Ci  # J1 is assumed to equal Jmax25!
  Je_Ci <- J1 * (Ci - Gam_Ci)/4/(Ci + 2*Gam_Ci)
  Jc_Ci <- Vcmax25_Ci * (Ci - Gam_Ci)/(Ci + Kc_Ci * (1 + Oi/Ko_Ci))
  
  Ag_Ci <- pmin(Je_Ci,Jc_Ci)
  An_Ci <- Ag_Ci - Rd
  
  Rubisco_lim <- which(Jc_Ci <= Je_Ci)
  RuBP_lim    <- which(Je_Ci < Jc_Ci)
  
  An_Ci[An_Ci < 0] <- NA
  
  
  
  ### 2) calculate Cc based on gm
  gm <- gmmax25
  
  An_Cc        <- An_Ci
  Vcmax_diff   <- 1
  Vcmax25_Cct1 <- Vcmax25_Ci
  i            <- 0
  imax         <- 30
  tr           <- TRUE
  sw           <- FALSE
  vstart       <- 1     # only used if sw switches to TRUE 

  while (Vcmax_diff > maxdiff & i < imax){
    
    i <- i + 1

    if (Ci_response){
      Cc <- Ci - An_Cc / (gm * (0.15 + 1.5* (1-exp(-Ci/38)) * exp(-Ci/460)))
    } else { # default case: gm is constant with Ci
      Cc <- Ci - An_Cc / gm
    }
  
    # constrain Cc > 0
    if (any(Cc < 0,na.rm=T)){
      Cc <- pmax(0,Cc)
      if (tr) warning("Cc falls below 0!!",immediate.=TRUE)
      tr <- FALSE  # display warning only the first time it happens
    }
  
  
    ### 3) refit Vcmax and Jmax
    if (all_Ci_based){  # take Ci-based Rubisco kinetic constants also for the Cc-based case
      Gam_Cc <- Gam_Ci
      Kc_Cc  <- Kc_Ci
      Ko_Cc  <- Ko_Ci
    }
  

    mod <- nls(An_Cc ~ c(pmin(J1 * (Cc - Gam_Cc)/4/(Cc + 2*Gam_Cc),Vcmax * (Cc - Gam_Cc)/(Cc + Kc_Cc * (1 + Oi/Ko_Cc))) - Rd),
               start=c(Vcmax=Vcmax25_Ci,J1=J1),algorithm="port")
    

    Vcmax25_Cc <- summary(mod)$coef[1,1]
    Jmax25_Cc  <- summary(mod)$coef[2,1]
    Ratio      <- Jmax25_Cc/Vcmax25_Cc   
  
    Vcmax_diff   <- abs(Vcmax25_Cc - Vcmax25_Cct1)
    Vcmax25_Cct1 <- Vcmax25_Cc
  
    
    ### 4) recalculate the An-Ci curve and evaluate the fit 
    Je_Cc <- Jmax25_Cc * (Cc - Gam_Cc)/4/(Cc + 2*Gam_Cc)
    Jc_Cc <- Vcmax25_Cc * (Cc - Gam_Cc)/(Cc + Kc_Cc * (1 + Oi/Ko_Cc))
  
    Ag_Cc <- pmin(Je_Cc,Jc_Cc)
    An_Cc <- Ag_Cc - Rd
    
    
    ### 5) 'security' switch: if An_Cc falls below 0, implement a smoother
    ## transition from An_Ci to An_Cc for the sake of numerical stability
    if (any(An_Cc < 0,na.rm=T) & !sw){
      sw <- TRUE
      v  <- vstart
    }
    
    if (sw){
      v <- max(v - (vstart/(0.8*imax)),0)
      An_Cc <- v*An_Ci + (1-v)*An_Cc
    }
  
    
  } # end Ci iteration loop
  
  
  
  Ci_eval <- Ci >= eval_Ci_range[1] & Ci <= eval_Ci_range[2]
  
  # root mean square error
  RMSE <- sqrt(mean((An_Cc[Ci_eval] - An_Ci[Ci_eval])^2,na.rm=TRUE))
  
  # mean absolute error
  MAE  <- mean(abs(An_Cc[Ci_eval] - An_Ci[Ci_eval]),na.rm=TRUE)
  
  eval_metrics        <- list(RMSE,MAE)
  names(eval_metrics) <- c("RMSE","MAE")
  
  return(list("Vcmax25_Cc"=Vcmax25_Cc,"Jmax25_Cc"=Jmax25_Cc,"Jmax_Vcmax_ratio"=Ratio,
              "eval_metrics"=eval_metrics))
  
}







#' Parameter adjustment of photosynthetic capacity for C4 vegetation.
#' 
#' @description Adjusts C4 photosynthetic parameters from their apparent (Ci-based) to true (Cc-based)
#'              values. 
#'                            
#' @param Ci            Leaf intercellular CO2 concentration (umol mol-1)                            
#' @param Vcmax25_Ci    Ci-based (apparent) maximum carboxylation rate at 25 degC (umol m-2 s-1)         
#' @param Jmax25_Ci     Ci-based (apparent) maximum electron transport rate at 25 degC (umol m-2 s-1)
#' @param Vpmax25_Ci    Ci-based (apparent) maximum PEP carboxylation rate (umol m-2 s-1)
#' @param gmmax25       Unstressed mesophyll conductance to CO2 transfer at 25 degC (mol m-2 s-1)
#' @param Rd            Day respiration (umol m-2 s-1)
#' @param k_Ci          Ci-based (apparent) initial slope of the CO2 response curve (umol m-2 s-1). Only used if \code{model = "Collatz_1992"}.
#' @param Vpr           PEP regeneration rate (umol m-2 s-1). Defaults to 80 umol m-2 s-1. 
#'                      Only used if \code{model = "vonCaemmerer_1999"} 
#' @param gbs           Bundle-sheath conductance to CO2 (mol m-2 s-1). Only used if \code{model = "vonCaemmerer_1999"}
#' @param x             Partitioning factor of electron transport rate. Only used if \code{model = "vonCaemmerer_1999"}
#' @param Kp_Ci         Ci-based Michaelis-Menten constant of PEP carboxylase for CO2 (umol mol-1).
#'                      Only used if \code{model = "vonCaemmerer_1999"}
#' @param Kp_Cc         Cc-based Michaelis-Menten constant of PEP carboxylase for CO2 (umol mol-1).
#'                      Only used if \code{model = "vonCaemmerer_1999"}
#' @param Ci_response   Does gm respond to intercellular CO2 concentration? Defaults to \code{FALSE}. 
#' @param all_Ci_based  Take Ci-based Rubisco kinetic constants also for the Cc-based case? Defaults to \code{FALSE}
#' @param eval_Ci_range A vector of length 2, indicating the minimum and maximum value of the Ci range for which the
#'                      agreement between the gm-implicit and gm-explicit An-Ci curves is evaluated.
#' @param maxdiff       Maximum difference between subsequent Vpmax_Cc (if \code{model="vonCaemmerer_1999"}) 
#'                      or k_Cc (if \code{model="Collatz_1992"}) estimates. Defaults to 0.05.                     
#' @param model         C4 Photosynthesis model. At the moment, either \code{"vonCaemmerer_1999"} or 
#'                      \code{"Collatz_1992"}. Note that parameter requirements depend on the model (see above).
#' 
#' @details 
#'          
#' @note      
#' Ci denotes Cm (CO2 concentration in the mesophyll cells) in C4 vegetation.
#' 
#' @return a list with the following elements:
#'         \item{Vpmax_Cc}{Cc-based maximum PEP carboxylation rate (umol m-2 s-1). Only if \code{model = "vonCaemmerer_1999"}.}
#'         \item{k_Cc}{Cc-based initial slope of the CO2 response curve (umol m-2 s-1). Only if \code{model = "Collatz_1992"}.}
#'         \item{eval_metrics}{Metrics evaluationg the difference between the implicit and explicit An-Ci curves.
#'                             At the moment, RMSE (root mean square error) and MAE (mean absolute error) are implemented}
#' 
#' @references Knauer et al. 2019: Effects of mesophyll conductance on vegetation responses to elevated
#'             CO2 concentrations in a land surface model. Global Change Biology.
#' 
#'             von Caemmerer, S., Furbank, R.T. 1999: Modeling C4 photosynthesis.
#'             In: Sage, R.F., Monson, R.K. (Eds.): C4 Plant Biology. Academic Press,
#'             Toronto, ON, Canada, 173-211.
#'             
#'             Collatz, G.J., Ribas-Carbo, M., Berry, J.: Coupled photosynthesis-stomatal conductance
#'             model for leaves of C4 plants. Functional Plant Biology, CSIRO, 1992, 19, 519-538.
#' 
#'             
#' @examples 
#' Ci <- seq(0,1500,1)   # range of Ci used for fitting
#' 
#' adjust.param.C4(Ci,Vcmax25_Ci=30,Jmax25_Ci=200,Vpmax25_Ci=60,gmmax25=1,Rd=0.3,
#'                 k_Ci=0.3,Vpr=40,gbs=0.003,x=0.4,Kp_Ci=80,Kp_Cc=80,
#'                 Ci_response=F,eval_Ci_range=c(0,1500),maxdiff=0.05,
#'                 model="Collatz_1992")
#' 
#' 
#' @export
adjust.param.C4 <- function(Ci,Vcmax25_Ci,Jmax25_Ci,Vpmax25_Ci,gmmax25,Rd,k_Ci,
                            Vpr=80,gbs=0.003,x=0.4,Kp_Ci=80,Kp_Cc=80,
                            Ci_response=F,eval_Ci_range=c(0,300),maxdiff=0.05,
                            model=c("vonCaemmerer_1999","Collatz_1992")){
  
  model <- match.arg(model)

  ## 1) calculate photosynthesis (An-Ci response), Ci = Cm in this case
  J1 <- Jmax25_Ci  # J1 is assumed to equal Jmax25!
  if (model == "vonCaemmerer_1999"){
    
    Vp_Ci <- pmin((Ci * Vpmax25_Ci) / (Ci + Kp_Ci), Vpr)
    Ac_Ci <- pmin(Vp_Ci + gbs*Ci - 0.5*Rd,Vcmax25_Ci-Rd) 
    Aj_Ci <- pmin(((x*J1) / 2 - 0.5*Rd + gbs*Ci),(((1-x)*J1)/3) - Rd)
    
    An_Ci <- pmin(Ac_Ci,Aj_Ci)
    
  } else if (model == "Collatz_1992"){  
    
    Ac_Ci <- rep(Vcmax25_Ci,length(Ci))  - Rd
    Aj_Ci <- (0.05 * (4.6*rep(1500,length(Ci))/2.3))  - Rd # this value is much higher than the one in JSBACH; the 1500 represents APAR
    Ae_Ci <- k_Ci * Ci - Rd
    
    An_Ci <- pmin(Ac_Ci,Aj_Ci,Ae_Ci)
  }
  
  Ac_lim <- which(Ac_Ci <= Aj_Ci)
  Aj_lim <- which(Aj_Ci < Ac_Ci)
  
  An_Ci[An_Ci < 0] <- NA
  
  
  
  ## 2) calculate Cc based on gm
  gm <- gmmax25
  

  if (model == "vonCaemmerer_1999"){
    Vpmax25_Cct1 <- Vpmax25_Ci
  } else if (model == "Collatz_1992"){
    k_Cct1 <- k_Ci
  }
  
  x_diff  <- 1  # where x is either Vpmax or k
  An_Cc   <- An_Ci
  i       <- 0
  imax    <- 50
  tr      <- TRUE
  
  while (x_diff > maxdiff & i < imax){
    
    i <- i + 1
  
    if (Ci_response){
      Cc <- Ci - An_Cc / (gm * (0.15 + 1.5* (1-exp(-Ci/38)) * exp(-Ci/460)))
    } else { # default case: constant gm with Ci
      Cc <- Ci - An_Cc / gm
    }
  
    # constrain Cc > 0
    
    if (any(Cc < 0,na.rm=T)){
      Cc <- pmax(0,Cc)
      if (tr) warning("Cc falls below 0!!",immediate.=TRUE)
      tr <- FALSE  # display warning only the first time it happens
    }
  
  
  
    ## 3) refit Vcmax and Jmax
    if (model == "vonCaemmerer_1999"){
      
      mod <- nls(An_Cc ~ c(pmin((pmin((Cc * Vpmax) / (Cc + Kp_Cc), Vpr) + gbs*Cc - 0.5*Rd),Vcmax25_Ci - Rd)),
                 start=c(Vpmax=Vpmax25_Ci),algorithm="port")   # Vcmax25_Ci would change marginally, is not fitted

      Vpmax25_Cc <- summary(mod)$coef[1,1]

      x_diff       <- abs(Vpmax25_Cc - Vpmax25_Cct1)
      Vpmax25_Cct1 <- Vpmax25_Cc
      
    
    } else if (model == "Collatz_1992"){
    
      mod <- nls(An_Cc ~ c(pmin(k * Cc - Rd, Vcmax25_Ci - Rd)),
                 start=c(k=1),algorithm="port")
    
      k_Cc <- summary(mod)$coef[1,1]
    
      x_diff   <- abs(k_Cc - k_Cct1)
      k_Cct1 <- k_Cc
      
    }
  
  

    ## 4) recalculate An using the refitted Vpmax and Vcmax
    if (model == "vonCaemmerer_1999"){
     
      Vp_Cc <- pmin((Cc * Vpmax25_Cc) / (Cc + Kp_Cc), Vpr)
      Ac_Cc <- pmin(Vp_Cc + gbs * Cc - 0.5*Rd,Vcmax25_Ci - Rd) 
      Aj_Cc <- pmin(((x*J1) / 2 - 0.5*Rd + gbs * Cc),(((1 - x) * J1)/3) - Rd)   # should not have an effect
    
      An_Cc <- pmin(Ac_Cc,Aj_Cc)
    
    } else if (model == "Collatz_1992"){
    
      Ac_Cc <- rep(Vcmax25_Ci,length(Cc)) - Rd
      Aj_Cc <- (0.05 * (4.6*rep(1500,length(Cc))/2.3)) - Rd # the 1500 represents APAR
      Ae_Cc <- k_Cc * Cc - Rd
    
      An_Cc <- pmin(Ac_Cc,Aj_Cc,Ae_Cc)
    }
  
  
  } # end Ci iteration loop
  
  
    
  Ci_eval <- Ci >= eval_Ci_range[1] & Ci <= eval_Ci_range[2]
  
  # root mean square error
  RMSE <- sqrt(mean((An_Cc[Ci_eval] - An_Ci[Ci_eval])^2,na.rm=TRUE))
  
  # mean absolute error
  MAE  <- mean(abs(An_Cc[Ci_eval] - An_Ci[Ci_eval]),na.rm=TRUE)
  
  eval_metrics        <- list(RMSE,MAE)
  names(eval_metrics) <- c("RMSE","MAE")
  
  if (model == "vonCaemmerer_1999"){
    
    return(list("Vpmax_Cc"=Vpmax25_Cc,"eval_metrics"=eval_metrics))
    
  } else if (model == "Collatz_1992"){
    
    return(list("k_Cc"=k_Cc,"eval_metrics"=eval_metrics))
    
  }
  
}



